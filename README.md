## Contents

This readme lists the files contained in the folder, the required steps for setup, how it works, functionality that could be added and some testing with dummy data.

---

The files neccesary for the vending program are:

1. *index.php*, *service,php* in the root folder, with two subfolders, *img* and *inc*
2. *vending.sql* to create and setup database also in root folder
3. *testing.sql* and *clean.sql* to import test data and to reset the products and coins to zero
3. 4 files in the inc folder, *db.inc.php*, *functions.inc.php*, *service-summary.inc.php* and *style1.css*
4. 7 images in the img folder

---

## Setup

1. Copy the files to a folder on the local webhost
2. Import **vending.sql** into a MySQL or MariaDB or equivalant - it will set up db and tables and import product and coin data
3. Modify **db.inc.php** to the correct password and username for the local database
4. Open the folder location from a browser, index.php should open
5. To import some test data, import **testing.sql** into the database
6. To remove all change and products from the machine import **clean.sql** into the database

---

## How it works

As much as possible it is meant to mimic the behaviour of a real machine while fulfilling the requirements in the [holded hub](https://github.com/holdedhub/coding-challenge)

1. The index is the customer's interface. They click on coins to add coins, and with sufficient credit can buy items.
2. There is a summary of how many products and how much money is in the machine at the bottom. Clicking on "Service Machine" allows the user to add/remove money and products like the vendor. 
3. The "credit" is NOT part of the machine´s money, and can be returned to the user at all times prior to purchase.
4. The user is shown at all times how much credit they have inserted, and there is a (hopefully) user friendly message displayed when they have inserted enough change or no change is available or the coin composition when they return coins or receive change
5. Sold out items cannot be selected
6. Items cannot be selected with insufficient credit
7. Once more than the price of the most expensive item (soda) is entered, the machine will not accept more money



---

### To-do list

Apart from making the code tidier / reducing and separating the two main files more into functions, usable classes & method & html output, things that could be added are:

1. The coin return function only calculates the most efficient change, so for example if the change is 0.90, the machine will give 3*0.25, 0.10 and 0.05. The minimum quantities to give change are based on this, so less than 3 25c will result in an insufficient change message
2. Prohibit anything bar exact change if the exact change only message is displayed - *the program is allowing purchases leading to negative coin values, although the total amount displayed on the service page is still correct*
3. Make a log of any transactions completed in the service page as I guess real vending machines record this info.
4. Allow a user to enter lots of change and buy multiple items and retrieve remaining change when needed
5. Allow users to add/remove multiple coins/bottles at the same time! (for example text boxes instead of radio button)

---

### Tests

**Test1**

1. Import testing.sql into the database
2. Click on 0.25c three times, verify that water becomes available.
3. Note the current machine change and total, 5 cent: 27 | 10 cent: 1 | 25 cent: 10 | 1 euro: 18 | 21.95
4. Purchase water. 
5. Verify that machine change is correct (should be 5 cent: 27 | 10 cent: 0 | 25 cent: 13 | 1 euro: 18  | 22.60
6. Verify that correct change only message is now displayed
7. Verify that water now displays as sold out

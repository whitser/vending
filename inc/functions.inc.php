<?php

// function to calculate the most efficient coin change for each possible value of change.
// note that decimal->binary conversions result in inexact decimal values requiring the use of a correcting variable for rounding errors
function giveChange($change=0) {
  $coins=array(0,0,0,0); // empty array of quantities of each coin required for change
  $epsilon=0.001;  // to account for rounding errors described in https://www.php.net/manual/en/language.types.float.php
  if ($change>=1-$epsilon) { $coins[3]=1; $change=$change-1; }
  while ($change>=0.25-$epsilon) {
    $coins[2]=$coins[2]+1;
    $change=$change-0.25;
  }
  while ($change>=0.10-$epsilon) {
    $coins[1]=$coins[1]+1;
    $change=$change-0.10;
  }
  if ($change>0.05-$epsilon) {
    $coins[0]=1;
  }
  return $coins;
}
?>
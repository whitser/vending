<?php
include ('inc/db.inc.php');
include ('inc/style1.css');
include ('inc/functions.inc.php');
$message="no info";
$exactchange=0;

// to see if there is sufficient change to give change - idealy should be function returning 1 or 0
$sufChangeq="select quantity-minchange avail from coins order by id asc";
$sufChange=mysqli_query($link,$sufChangeq);
while ($row=mysqli_fetch_array($sufChange)) {
  $diffs[]=$row['avail'];
}
foreach ($diffs as $diff) {
  if ($diff<0) {
    $message="EXACT CHANGE ONLY PLEASE";
    $exactchange=1;
  }
}
// end sufficient change

// purchase item - to become function / class
if (isset($_POST['purchaseItem'])) {
  $id=$_POST['purchaseItem'];
  $query="select * from products where id=$id";
  $result=mysqli_query($link,$query);
  // calculate total user credit, cost of item being purchased and calculate change
  while ($row = mysqli_fetch_array($result)) {
    $cost=$row['price'];
    $item=$row['name'];
    $query="select sum(val*quantity) tot from customer";
    $res=mysqli_query($link,$query);
    while ($row = mysqli_fetch_array($res)) {
      $tot=$row['tot'];
    }
    $change=$tot-$cost;
    $custCoins="select * from customer";
    $custResult=mysqli_query($link,$custCoins);
    // transfer user credit to the machine change
    while ($row=mysqli_fetch_array($custResult)) {
      $updateCoinsq="update coins set quantity=quantity+".$row['quantity']." where val=".$row['val'];
      $updateCoins=mysqli_query($link,$updateCoinsq);
    }
    // remove the product from the machine (or subtract it from the database!)
    $updProduct=mysqli_query($link,"update products set quantity=quantity-1 where id=$id");
    // remove all coins from the user credit - possibly could be done earlier to mimic real life
    $updCustomer=mysqli_query($link,"update customer set quantity=0");
    $message="Please take your $item";
    if ($change > 0) {
        // function to calculate composition of change
      $coinChange=giveChange($change);
      $x=1;
      foreach ($coinChange as $denomination) {
        if ($denomination>0) {
          $query="update coins set quantity=quantity-$denomination where id=$x";
          $res=mysqli_query($link,$query);
        }
        $x++;
      }
      $message .= " and your $change change: ";
      if ($coinChange[0]>0) { $message .= " 5c *".$coinChange[0]." | "; }
      if ($coinChange[1]>0) { $message .= " 10c *".$coinChange[1]." | "; }
      if ($coinChange[2]>0) { $message .= " 25c *".$coinChange[2]." | "; }
      if ($coinChange[3]>0) { $message .= " 1 eur *".$coinChange[3]." | "; }
      
    } 
  }  
}
// end purchase item
if (isset($_POST['coin'])) {
  $query="select sum(val*quantity) tot from customer";
  $res=mysqli_query($link,$query);
  while ($row = mysqli_fetch_array($res)) {
    $tot=$row['tot'];
  }
  if ($tot<1.50) {
    $coin=$_POST['coin'];
    $query="update customer set quantity=quantity+1 where id=$coin";
    $result=mysqli_query($link,$query);
  } else {
    $message="Already sufficient credit entered";
  }
}

if (isset($_POST['coinret'])) {
$message = "CHANGE RETURNED:";
$query="update customer set quantity=0";
$result=mysqli_query($link,$query);

if ($_POST['euro']>0) { $message .= "<br />1 EURO *".$_POST['euro']; }
if ($_POST['twfivec']>0) { $message .= "<br />25 cent *".$_POST['twfivec']; }
if ($_POST['tenc']>0) { $message .= "<br />10 cent *".$_POST['tenc']; }
if ($_POST['fivec']>0) { $message .= "<br />5 cent *".$_POST['fivec']; }

}



$query="select quantity from customer order by id asc";
if (!mysqli_query($link,$query)) {
  die("error with query:$query");
} else {
  $result=mysqli_query($link,$query);
  while ($row = mysqli_fetch_array($result)) {
    $coins[]=$row['quantity'];
  }

}

$query="select sum(val*quantity) tot from customer";
$res=mysqli_query($link,$query);
while ($row = mysqli_fetch_array($res)) {
    $tot=$row['tot'];
}
if ($tot>0) { $disabled=''; } else { $disabled='disabled'; }

?>
<div class="service-frame">
<div class="heading">Welcome to the Vending Machine</div>
<br />
<br />
<!-- form/html output to click on coin image to enter user credit -->
<div class="items-container">
<form method="POST" target="">
<div class="coins">
	<span>
		<input type="image" title="Insert 5c" src="img/5cent.png" border="0" alt="5c" height="40" width="40" />
		<input type="hidden" value="1" name="coin" />
	</span>
	<span><?php echo $coins[0]; ?></span>
	</div>
</form>

<form method="POST" target="">
	<div class="coins">
	<span>
		<input type="image" title="Insert 10c" src="img/10cent.png" border="0" alt="10c" height="40" width="40" />
		<input type="hidden" value="2" name="coin" />
	</span>
	<span><?php echo $coins[1]; ?></span>
	</div>
</form>

<form method="POST" target="">
	<div class="coins">
	<span>
		<input type="image" title="Insert 25c" src="img/25cent.png" border="0" alt="25c" height="40" width="40" />
		<input type="hidden" value="3" name="coin" />
	</span>
	<span><?php echo $coins[2]; ?></span>
	</div>
</form>

<form method="POST" target="">
	<div class="coins">
	<span>
		<input type="image" title="Insert euro" src="img/1euro.png" border="0" alt="1eur" height="40" width="40" />
		<input type="hidden" value="4" name="coin" />
	</span>
	<span><?php echo $coins[3]; ?></span>
	</div>
</form>
<div>Click on coins to add credit</div>
</div>

<!-- form/html output to return user credit prior to purchasing an item -->
<form name="returnCredit"  method="POST" target="">
	<div>
	<span>
		<input type="submit" value="RETURN COINS" alt="Submit" <?php echo $disabled; ?> />
		<input type="hidden" value="1" name="coinret" />
    <input type="hidden" value="<?php echo $coins[0]; ?>" name="fivec" />
    <input type="hidden" value="<?php echo $coins[1]; ?>" name="tenc" />
    <input type="hidden" value="<?php echo $coins[2]; ?>" name="twfivec" />
    <input type="hidden" value="<?php echo $coins[3]; ?>" name="euro" />
	</span>
	<span>Click here to return your coins</span>
	</div>
</form>

<div>CREDIT: EUR<?php echo $tot; ?></div>
<div><span>USER INFO: </span><?php echo $message; ?></div>

<div>Available Products</div>

<?php
$query="select name,id,quantity,price from products order by id asc";
$result=mysqli_query($link,$query);
while ($row=mysqli_fetch_array($result)) {
  $id=$row['id'];
  $dis="disabled$id";
  $val="value$id";

  if ($row['quantity']==0) {
    ${$val} = 'SOLD OUT';
    ${$dis}="disabled";
  }
  else {
    ${$val}=$row['name'].' '.$row['price']; 
    if ($row['price']>$tot) {  ${$dis}="disabled"; }
    else { ${$dis}=''; }
  }
}
?>
<!-- html output for selecting products  -->
<div class="items-container">
<form action="" name="purchaseItem1" method="POST"><div class="items"><img src="img/water.png" alt="water"/><br /><input type="hidden" name="purchaseItem" value="1" /><input type="submit" value="<?php echo $value1; ?>" <?php echo $disabled1; ?> /></div></form>
<form action="" name="purchaseItem1" method="POST"><div class="items"><img src="img/juice.png" alt="juice" /><br /><input type="hidden" name="purchaseItem" value="2" /><input type="submit" value="<?php echo $value2; ?>" <?php echo $disabled2; ?> /></div></form>
<form action="" name="purchaseItem1" method="POST"><div class="items"><img src="img/soda.png" alt="soda" /><br /><input type="hidden" name="purchaseItem" value="3" /><input type="submit" value="<?php echo $value3; ?>" <?php echo $disabled3; ?> /></div></form>
</div>
<br />
<div class="items-container"><form action="service.php" method="POST"><div><input type="submit" name="SERVICE" value="SERVICE MACHINE" />&nbsp;&nbsp;</div></form>
<div>Summary of Avail Items & Change:<br />
<?php include('inc/service-summary.inc.php'); ?>
<br />Click button to add/remove items/change
</div>
</div>
</div></div>

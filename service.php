<?php
include ('inc/db.inc.php');
include ('inc/style1.css');
?>
<div class="service-frame">
<div class="heading">Welcome to the service page</div>
<br />
<?php
// code to add/remove products
if (isset($_POST['addRemProduct'])) {
  $action=$_POST['action'];
  $id=$_POST['addRemProduct'];
  if ($action=="add$id") { $addval=1; }
  if ($action=="rem$id") { $addval=-1; }
  $query="update products set quantity = quantity+$addval where id=$id";
  $result  = mysqli_query($link,$query);
  //die($query);
}
// end code add/remove products

// code to add/remove coins
if (isset($_POST['addRemCoin'])) {
  $action=$_POST['action'];
  $id=$_POST['addRemCoin'];
  if ($action=="addc$id") { $addval=1; }
  if ($action=="remc$id") { $addval=-1; }
  $query="update coins set quantity = quantity+$addval where id=$id";
  $result  = mysqli_query($link,$query);
  //die($query);
}
// end code add/remove coins

// display total money
$query="select sum(val*quantity) tot from coins";
$res=mysqli_query($link,$query);
while ($row = mysqli_fetch_array($res)) {
    $tot=$row['tot'];
}
echo "<div>Total of money in machine: $tot </div>";


echo "Add/Remove Products";

// select products

$query="select name,id,quantity,price from products order by id asc";
$result=mysqli_query($link,$query);
while ($row=mysqli_fetch_array($result)) {
  $disabled='';
  $id=$row['id'];
  $name=$row['name'];
  $quantity=$row['quantity'];
  if ($quantity==0) { $disabled='disabled'; }
  echo "<div class=\"rTableRow\"><form name=\"$name\" target=\"\" method=\"POST\">";
  echo "<span>$name:</span><span>&nbsp;&nbsp;&nbsp;&nbsp;$quantity&nbsp;&nbsp;&nbsp;&nbsp;</span><span>";
  echo "<input type=\"hidden\" name=\"addRemProduct\" value=\"$id\" />";
  echo "<label for=\"add$id\">ADD</label> <input type=\"radio\" name=\"action\" id=\"add$id\" value=\"add$id\" />";
  echo "&nbsp;&nbsp;&nbsp;<label for=\"rem$id\">REMOVE</label> <input type=\"radio\" name=\"action\" id=\"rem$id\" value=\"rem$id\" $disabled />";
  echo "<input type=\"submit\" />";
  echo "</span></form></div>";
}
?>
<br />
<br />
<?php
// select money
$query="select name,id,val,quantity, minchange from coins order by id asc";
$result=mysqli_query($link,$query);
while ($row=mysqli_fetch_array($result)) {
  $disabled='';
  $id=$row['id'];
  $name=$row['name'];
  $quantity=$row['quantity'];
  $coinTot=$quantity*$row['val'];
  if ($quantity==0) { $disabled='disabled'; }
  echo "<div><form name=\"$name\" target=\"\" method=\"POST\">";
  echo "<span>$name:</span><span>&nbsp;&nbsp;&nbsp;&nbsp;$quantity&nbsp;&nbsp;&nbsp;&nbsp;</span><span>";
  echo "<input type=\"hidden\" name=\"addRemCoin\" value=\"$id\" />";
  echo "<label for=\"addc$id\">ADD</label> <input type=\"radio\" name=\"action\" id=\"addc$id\" value=\"addc$id\" />";
  echo "&nbsp;&nbsp;&nbsp;<label for=\"remc$id\">REMOVE</label> <input type=\"radio\" name=\"action\" id=\"remc$id\" value=\"remc$id\" $disabled />";
  echo "<input type=\"submit\" />";
  echo "</span></form></div>";
}
?>
<form action="index.php" method="POST"><div><input type="submit" name="SERVICE" value="CLOSE MACHINE" />&nbsp;&nbsp;Click to return to customer interface</div></form>
<!-- footer start-->
</div>

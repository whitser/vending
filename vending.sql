create database if not exists vending;

create table if not exists vending.products (
  id int NOT NULL auto_increment primary key,
  name char(5) not null default "EMPTY",
  price decimal(3,2) not null default 0.00,
  quantity int not null default 0
) default character set utf8;

create table if not exists vending.coins (
  id int NOT NULL auto_increment primary key,
  name char(25) not null default "UNKNOWN",
  val decimal(3,2) not null default 0.00,
  quantity int not null default 0,
  minchange int not null default 0
) default character set utf8;

create table if not exists vending.customer (
  id int NOT NULL auto_increment primary key,
  name char(25) not null default "UNKNOWN",
  val decimal(3,2) not null default 0.00,
  quantity int not null default 0
) default character set utf8;

insert ignore into vending.products (name,price)
values 
("Water",0.65),
("Juice",1.00),
("soda",1.50);

insert ignore into vending.coins (name,val,minchange)
values
("5 cent", 0.05,1),
("10 cent", 0.10,2),
("25 cent","0.25",3),
("1 euro", "1.00",1);

insert ignore into vending.customer (name,val)
values
("5 cent", 0.05),
("10 cent", 0.10),
("25 cent","0.25"),
("1 euro", "1.00");

